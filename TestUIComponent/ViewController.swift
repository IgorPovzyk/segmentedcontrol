//
//  ViewController.swift
//  TestUIComponent
//
//  Created by Ihor Povzyk on 9/18/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var control:CustomSegmentedControl? = nil
    var items = ["0","1","2"]
    
    @IBOutlet weak var deleteAt: UITextField!
    @IBOutlet weak var insertAt: UITextField!
    @IBOutlet weak var itemTitle: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        control = CustomSegmentedControl(items: items, xPosition: 100, yPosition: 50)
        control?.addTarget(self, action: #selector(ViewController.segmentValueChanged(sender:)), for: .valueChanged)
        control?.tag = 777
        self.view.addSubview(control!)
        
    }
    
    func segmentValueChanged(sender: AnyObject?){
        NSLog("Code added selected index - \(control?.selectedIndex)")
        
    }
    
    @IBAction func addPressed(_ sender: AnyObject) {
        if insertAt.text != nil && (insertAt.text?.characters.count)! > 0 && itemTitle.text != nil && (itemTitle.text?.characters.count)! > 0 {
            let index = Int.init(insertAt.text!)
            if index != nil {
                control?.addLabel(atPos: Int.init(insertAt.text!)!, newLabel: itemTitle.text!)
            }

        }
    }
    
    @IBAction func changed(_ sender: AnyObject) {
        let s = sender as! CustomSegmentedControl
        NSLog("Storyboard added selected index - \(s.selectedIndex)")
    }
    
    @IBAction func removePressed(_ sender: AnyObject) {
        if deleteAt.text != nil && (deleteAt.text?.characters.count)! > 0 {
            let index = Int.init(deleteAt.text!)
            if index != nil {
                control?.delLabel(atPos: Int.init(deleteAt.text!)!)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


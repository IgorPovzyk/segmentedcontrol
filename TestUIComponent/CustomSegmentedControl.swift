//
//  CustomSegmentedControl.swift
//  TestUIComponent
//
//  Created by Ihor Povzyk on 9/18/16.
//  Copyright © 2016 Ihor Povzyk. All rights reserved.
//

import UIKit


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}


@IBDesignable class CustomSegmentedControl: UIControl{
    
    
    
    private var labels = [UILabel]()
    private var xPosition:CGFloat = 0
    private var yPosition:CGFloat = 0

    private var items: [String] = ["0","1"] {
        didSet {
            setupView()
            setupLabels()
        }
    }
    var selectedIndex : Int = -1 {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    init (items:[String],xPosition:CGFloat, yPosition:CGFloat) {
        self.items = items
        self.xPosition = xPosition
        self.yPosition = yPosition
        super.init(frame: CGRect(x: xPosition, y: yPosition, width: CGFloat((items.count)*50+(items.count)*5), height: CGFloat(40)))
        setupView()
        setupLabels()
    }
    
    override init(frame: CGRect) {
        let f = CGRect(x: 0, y: 0, width: 100, height: 40)
        super.init(frame: f)
        setupView()
        setupLabels()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
        setupView()
        setupLabels()
    }
    
    
    @IBInspectable var selectedLabelColor : UIColor = UIColor.white {
        didSet {
            setSelectedColors()
        }
    }
    
    @IBInspectable var unselectedLabelColor : UIColor = UIColor.init(netHex: 0xB2FF59) {
        didSet {
            setSelectedColors()
        }
    }
    
    
    @IBInspectable var unselectedBackgroundLabelColor : UIColor = UIColor.init(netHex: 0xF5F5F5) {
        didSet {
            setSelectedColors()
        }
    }
    
    
    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet {
            setSelectedColors()
        }
    }
    
    @IBInspectable var font : UIFont! = UIFont.systemFont(ofSize: 12) {
        didSet {
            setFont()
        }
    }
    

    
    private func setupView(){
        if layer.sublayers != nil {
            for layer in layer.sublayers! {
                layer.removeFromSuperlayer()
            }
        }
        if items.count > 0 {
            layer.cornerRadius = frame.height / 2
            
            bounds = CGRect(x: xPosition, y: yPosition, width: CGFloat((items.count)*50+(items.count)*7), height:CGFloat( 40))
            layer.bounds = CGRect(x: 0, y: 0, width: CGFloat((items.count)*50+(items.count)*7), height:CGFloat( 40))
            backgroundColor = UIColor.clear
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: 7, y: 20))
            path.addLine(to: CGPoint(x: layer.bounds.width-7, y: 20))
            
            let shapeLayer = CAShapeLayer(layer: layer)
            shapeLayer.path = path.cgPath
            
            shapeLayer.strokeColor = UIColor.black.cgColor
            shapeLayer.lineWidth = 1.0
            shapeLayer.fillColor = UIColor.black.cgColor
            
            layer.addSublayer(shapeLayer)
            
            setupLabels()
        }
    }
    
    private func setupLabels(){
        
        for label in labels {
            label.removeFromSuperview()
        }
        
        labels.removeAll(keepingCapacity: false)
        
        for index in 0..<items.count {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            
            label.text = items[index]
            label.layer.backgroundColor = unselectedBackgroundLabelColor.cgColor
            label.textAlignment = .center
            label.font = UIFont(name: "Avenir-Black", size: 15)
            label.textColor = index == 0 ? selectedLabelColor : unselectedLabelColor
            label.layer.borderColor = UIColor.black.cgColor
            label.layer.borderWidth = 1
            label.translatesAutoresizingMaskIntoConstraints = false
            label.layer.cornerRadius = label.bounds.height/2
            self.addSubview(label)
            
            labels.append(label)
        }
        
        setupConstraints(items: labels, mainView: self, padding: 10)
    }
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        displayNewSelectedIndex()
        
    }
    
    
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        let location = touch.location(in: self)
        
        var calculatedIndex : Int?
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                if index == selectedIndex {
                    calculatedIndex = -1
                }
                else {
                    calculatedIndex = index
                }
            }
        }
        
        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!            
            sendActions(for: .valueChanged)
        }
        return false
    }
    
    
    
    private func displayNewSelectedIndex(){
        for (index, item) in labels.enumerated() {
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: .curveLinear, animations: {
                item.textColor = self.unselectedLabelColor
                item.text = self.items[index]
                item.layer.backgroundColor = self.unselectedBackgroundLabelColor.cgColor
                item.layer.bounds.size.width = 40
                }, completion: nil)
        }
        if selectedIndex > -1 {
            let label = labels[selectedIndex]
            label.textColor = selectedLabelColor
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: .curveLinear, animations: {
                label.layer.backgroundColor = self.unselectedLabelColor.cgColor
                label.text = "\(self.items[self.selectedIndex]) days"
                label.textColor = self.selectedLabelColor
                label.layer.bounds.size.width = 70                
                }, completion: nil)
        }
        
    }
    
    
    private func setupConstraints(items: [UIView], mainView: UIView, padding: CGFloat) {
        
        mainView.removeConstraints(mainView.constraints)
        
        for (index, button) in items.enumerated() {
            
            let topConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0)
            
            let bottomConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0)
            
            var rightConstraint : NSLayoutConstraint!
            
            if index == items.count - 1 {
                
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: 0)
                
            }else{
                
                let nextButton = items[index+1]
                rightConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: nextButton, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: -padding)
            }
            
            var leftConstraint : NSLayoutConstraint!
            
            if index == 0 {
                
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: mainView, attribute: NSLayoutAttribute.left, multiplier: 1.0, constant: 0)
                
            }else{
                
                let prevButton = items[index-1]
                leftConstraint = NSLayoutConstraint(item: button, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: prevButton, attribute: NSLayoutAttribute.right, multiplier: 1.0, constant: padding)
                
                let firstItem = items[0]
                
                let widthConstraint = NSLayoutConstraint(item: button, attribute: .width, relatedBy: NSLayoutRelation.equal, toItem: firstItem, attribute: .width, multiplier: 1.0  , constant: 0)
                
                mainView.addConstraint(widthConstraint)
            }
            mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
        }
    }
    
    
    func addLabel(atPos:Int,newLabel:String){
        if items.count >= atPos {
        items.insert(newLabel, at: atPos)        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: .curveLinear, animations: {
            self.setupView()
            self.setupLabels()
            }, completion: nil)
        layoutSubviews()
        }
    }
    
    
    func delLabel(atPos:Int){
        if items.count > atPos {
            items.remove(at: atPos)
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, options: .curveLinear, animations: {
                self.setupView()
                self.setupLabels()
                
                }, completion: nil)
        }
        layoutSubviews()
    }
    
    private func setSelectedColors(){
        for item in labels {
            item.textColor = unselectedLabelColor
            item.layer.backgroundColor = unselectedBackgroundLabelColor.cgColor
            item.layer.borderColor = borderColor.cgColor
        }
    }
    
    private func setFont(){
        for item in labels {
            item.font = font
        }
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
